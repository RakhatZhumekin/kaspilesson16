package Project1;

import Project1.entity.Client;
import Project1.entity.Orders;
import Project1.entity.Product;
import Project1.entity.Manufacturer;
import Project1.service.*;

import java.time.LocalDate;
import java.util.*;

public class App {

    ClientService clientService = new ClientService();
    OrdersService ordersService = new OrdersService();
    ProductService productService = new ProductService();
    ManufacturerService manufacturerService = new ManufacturerService();

    public static void main(String[] args) {
        new App().go();
    }

    public void go() {
        insertOneClient();

        insertOneManufacturer();

        insertOneProduct();

        insertOneOrder();

        List<Product> products = getAllProducts();
        for (int i = 0; i < products.size(); i++) {
            System.out.println(i + " " + products.get(i).getName());
        }

        Map<Product, Integer> productsWithCounts = getMostPopularProducts(5);
        Iterator it = productsWithCounts.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            Product product = (Product)entry.getKey();
            Integer count = (Integer)entry.getValue();
            System.out.println(product.getName() + " " + count);
        }
    }

    public void insertOneClient() {
        Client client = new Client();
        client.setName("Emily");
        client.setAddress("Wall Street");

        clientService.persist(client);
    }

    public void insertOneManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Apple");
        manufacturer.setSiteLink("apple.com");
        manufacturerService.persist(manufacturer);
    }

    public void insertOneProduct() {
        List<Manufacturer> manufacturers = manufacturerService.findAll();
        Manufacturer apple = manufacturers.get(0);
        Product product = new Product();
        product.setName("iPod");
        product.setPrice(500);
        product.setManufacturer(apple);

        apple.getProducts().add(product);

        productService.persist(product);
    }

    public void insertOneOrder() {
        List<Client> clients = clientService.findAll();
        List<Product> products = productService.findAll();

        Client client1 = clients.get(0);
        Product product1 = new Product();
        for (int i = 0; i < products.size(); i++) {
            if (products.get(i).getName().equals("iPad")) {
                product1 = products.get(i);
                break;
            }
        }

        Orders order = new Orders();
        order.setClient(client1);
        order.setOrderTime(LocalDate.of(2021, 3, 15));
        order.getProducts().add(product1);

        product1.getOrders().add(order);

        ordersService.persist(order);
    }

    public List<Product> getAllProducts() {
        List<Product> products = productService.findAll();
        return products;
    }

    public Map<Product, Integer> getMostPopularProducts(int limit) {
        Map<Product, Integer> productsWithCount = new LinkedHashMap<>();

        List<Product> products = getAllProducts();

        List<Integer> orderCounts = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            orderCounts.add(products.get(i).getOrders().size());
        }

        Collections.sort(orderCounts, Collections.reverseOrder());

        int iterations = Math.min(limit, orderCounts.size());
        for (int i = 0; i < iterations; i++) {
            for (int j = 0; j < products.size(); j++) {
                if (products.get(j).getOrders().size() == orderCounts.get(i)) {
                    productsWithCount.put(products.get(j), orderCounts.get(i));
                }
            }
        }

        return productsWithCount;
    }

    public String getGreeting() {
        return "Hello";
    }
}
