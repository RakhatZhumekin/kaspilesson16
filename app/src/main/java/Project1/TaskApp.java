package Project1;

import Project1.entity.Employee;
import Project1.entity.Department;
import Project1.util.HibernateUtil;
import javax.persistence.*;

import org.hibernate.Session;

import java.time.LocalDate;
import java.util.List;

public class TaskApp {
    public static void main(String[] args) {
        System.out.println("Begin");

        insertDepartments();

        insertEmployees();

        List<Employee> retiredAgeEmployees = getOfRetirementAge();
        retiredAgeEmployees.forEach(e -> System.out.println(e.getFirstName()));

        List<Employee> birthdayEmployees = getBirthdayEmployees();
        birthdayEmployees.forEach(e -> System.out.println(e.getFirstName()));

        updateEmployeeSalary(152, 35000);

        List<Integer> topSalaries = getTopSalaries(3);
        topSalaries.forEach(s -> System.out.println(s));
    }

    // Task 1
    private static void insertDepartments() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Department department1 = new Department();
        department1.setName("IT");

        Department department2 = new Department();
        department2.setName("HR");

        Department department3 = new Department();
        department3.setName("Finance");

        session.save(department1);
        session.save(department2);
        session.save(department3);

        session.getTransaction().commit();

        HibernateUtil.shutdown();

        System.out.println("Inserted departments successfully");
    }

    // Task 2
    private static void insertEmployees() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Department> departments = getDepartments();
        Department department1 = departments.get(0);
        Department department2 = departments.get(1);
        Department department3 = departments.get(2);

        Employee employee1 = new Employee();
        employee1.setFirstName("Brian");
        employee1.setGender(Employee.Gender.MALE);
        employee1.setBirthDate(LocalDate.of(1995, 4, 15));
        employee1.setSalary(5000);
        employee1.setDepartment(department1);

        Employee employee2 = new Employee();
        employee2.setFirstName("Marie");
        employee2.setGender(Employee.Gender.FEMALE);
        employee2.setBirthDate(LocalDate.of(1962, 1, 30));
        employee2.setSalary(10000);
        employee2.setDepartment(department2);

        Employee employee3 = new Employee();
        employee3.setFirstName("Shawn");
        employee3.setGender(Employee.Gender.MALE);
        employee3.setBirthDate(LocalDate.of(1956, 4, 7));
        employee3.setSalary(10500);
        employee3.setDepartment(department3);

        session.save(employee1);
        session.save(employee2);
        session.save(employee3);

        session.getTransaction().commit();
        HibernateUtil.shutdown();

        System.out.println("Inserted employees successfully");
    }

    // Task 3
    private static List<Employee> getOfRetirementAge() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Employee> employees = session.createQuery("SELECT e from Employee e " +
                "where (extract(year from age(e.birthDate)) >= 63 and e.gender = 'MALE') " +
                "or (extract(year from age(e.birthDate)) >= 58 and e.gender = 'FEMALE')")
                .getResultList();

        session.getTransaction().commit();

        return employees;
    }

    // Task 4
    private static List<Employee> getBirthdayEmployees() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Employee> employees = session.createQuery("SELECT e FROM Employee e " +
                "where (EXTRACT(month from birthDate) = EXTRACT(month from CURRENT_DATE)) " +
                "and (EXTRACT(day from birth_date) = EXTRACT(day from CURRENT_DATE))")
                .getResultList();

        session.getTransaction().commit();

        return employees;
    }

    // Task 5
    private static void updateEmployeeSalary(int id, int salary) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("UPDATE Employee SET salary = ?1 where id = ?2");
        query.setParameter(1, salary);
        query.setParameter(2, id);
        query.executeUpdate();

        session.getTransaction().commit();

        System.out.println("Update successful");
    }

    // Task 6
    private static List<Integer> getTopSalaries(int limit) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Integer> topSalaries = session.createQuery("SELECT salary FROM Employee " +
                "order by salary desc")
                .setMaxResults(limit)
                .getResultList();

        session.getTransaction().commit();
        return topSalaries;
    }

    // Helper method for Task 2
    private static List<Department> getDepartments() {

        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        List<Department> departments = session.createQuery("SELECT d FROM Department d").getResultList();

        session.getTransaction().commit();

        return departments;
    }
}
