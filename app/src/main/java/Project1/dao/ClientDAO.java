package Project1.dao;

import org.hibernate.Session;

import Project1.entity.Client;

import java.util.List;

public interface ClientDAO<Client, Integer> {
    public Session openSession();
    public void closeSession();

    public void persist(Client client);

    public void update(Client client);

    public Client findById(Integer id);

    public void delete(Client client);

    public List<Client> findAll();

    public void deleteAll();
}