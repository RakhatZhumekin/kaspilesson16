package Project1.dao;

import Project1.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import Project1.entity.Client;

import java.util.List;

public class ClientDAOImpl implements ClientDAO<Client, Integer>{
    private Session currentSession;

    private Transaction currentTransaction;

    @Override
    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    @Override
    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Client client) {
        getCurrentSession().save(client);
    }

    @Override
    public void update(Client client) {
        getCurrentSession().update(client);
    }

    @Override
    public Client findById(Integer id) {
        return getCurrentSession().get(Client.class, id);
    }

    @Override
    public void delete(Client client) {
        getCurrentSession().delete(client);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Client> findAll() {
        return getCurrentSession().createQuery("from Client").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(c -> delete(c));
    }
}