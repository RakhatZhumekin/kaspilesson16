package Project1.dao;

import org.hibernate.Session;

import Project1.entity.Manufacturer;

import java.util.List;

public interface ManufacturerDAO<Manufacturer, Integer> {
    public Session openSession();
    public void closeSession();

    public void persist(Manufacturer manufacturer);

    public void update(Manufacturer manufacturer);

    public Manufacturer findById(Integer id);

    public void delete(Manufacturer manufacturer);

    public List<Manufacturer> findAll();

    public void deleteAll();
}