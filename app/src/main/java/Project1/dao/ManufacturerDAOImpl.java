package Project1.dao;

import Project1.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import Project1.entity.Manufacturer;

import java.util.List;

public class ManufacturerDAOImpl implements ManufacturerDAO<Manufacturer, Integer>{
    private Session currentSession;

    private Transaction currentTransaction;

    @Override
    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    @Override
    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Manufacturer manufacturer) {
        getCurrentSession().save(manufacturer);
    }

    @Override
    public void update(Manufacturer manufacturer) {
        getCurrentSession().update(manufacturer);
    }

    @Override
    public Manufacturer findById(Integer id) {
        return getCurrentSession().get(Manufacturer.class, id);
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        getCurrentSession().delete(manufacturer);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Manufacturer> findAll() {
        return getCurrentSession().createQuery("from Manufacturer").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(m -> delete(m));
    }
}