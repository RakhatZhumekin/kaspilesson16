package Project1.dao;

import org.hibernate.Session;

import Project1.entity.Orders;

import java.util.List;

public interface OrdersDAO<Orders, Integer> {
    public Session openSession();
    public void closeSession();

    public void persist(Orders order);

    public void update(Orders order);

    public Orders findById(Integer id);

    public void delete(Orders order);

    public List<Orders> findAll();

    public void deleteAll();
}