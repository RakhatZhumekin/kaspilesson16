package Project1.dao;

import Project1.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import Project1.entity.Orders;

import java.util.List;

public class OrdersDAOImpl implements OrdersDAO<Orders, Integer>{
    private Session currentSession;

    private Transaction currentTransaction;

    @Override
    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    @Override
    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Orders order) {
        getCurrentSession().save(order);
    }

    @Override
    public void update(Orders order) {
        getCurrentSession().update(order);
    }

    @Override
    public Orders findById(Integer id) {
        return getCurrentSession().get(Orders.class, id);
    }

    @Override
    public void delete(Orders order) {
        getCurrentSession().delete(order);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Orders> findAll() {
        return getCurrentSession().createQuery("from Orders").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(o -> delete(o));
    }
}