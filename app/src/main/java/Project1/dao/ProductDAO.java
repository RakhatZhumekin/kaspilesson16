package Project1.dao;

import org.hibernate.Session;

import Project1.entity.Product;

import java.util.List;
import java.util.Map;

public interface ProductDAO<Product, Integer> {
    public Session openSession();
    public void closeSession();

    public void persist(Product product);

    public void update(Product product);

    public Product findById(Integer id);

    public void delete(Product product);

    public List<Product> findAll();

    public void deleteAll();

//    public Map<Product, Integer> findMostPopularProducts(int limit);
}