package Project1.dao;

import Project1.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import Project1.entity.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductDAOImpl implements ProductDAO<Product, Integer>{
    private Session currentSession;

    private Transaction currentTransaction;

    @Override
    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    @Override
    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Product product) {
        getCurrentSession().save(product);
    }

    @Override
    public void update(Product product) {
        getCurrentSession().update(product);
    }

    @Override
    public Product findById(Integer id) {
        return getCurrentSession().get(Product.class, id);
    }

    @Override
    public void delete(Product product) {
        getCurrentSession().delete(product);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Product> findAll() {
        return getCurrentSession().createQuery("from Product").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(p -> delete(p));
    }

//    @Override
//    public Map<Product, Integer> findMostPopularProducts(int limit) {
//        Map<Product, Integer> productsWithCounts = new HashMap<>();
//        List<Product> products = getCurrentSession().createQuery("select p from Product p, order_details " +
//                "where p.id = product_id " +
//                "group by p.id, product_id " +
//                "order by count(product_id) desc")
//                .setMaxResults(limit)
//                .getResultList();
//
//        List<Integer> counts = getCurrentSession().createQuery("select count(product_id) from Product, order_details " +
//                "where id = product_id " +
//                "group by id, product_id " +
//                "order by count(product_id) desc")
//                .setMaxResults(limit)
//                .getResultList();
//
//        for (int i = 0; i < products.size(); i++) {
//            productsWithCounts.put(products.get(i), counts.get(i));
//        }
//
//        return productsWithCounts;
//    }
}