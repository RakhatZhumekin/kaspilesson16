package Project1.service;

import Project1.dao.ClientDAO;
import Project1.dao.ClientDAOImpl;
import Project1.entity.Client;

import java.util.List;

public class ClientService {

    private static ClientDAO<Client, Integer> clientDAO;

    public ClientService() {
        clientDAO = new ClientDAOImpl();
    }

    public void persist(Client client) {
        clientDAO.openSession();
        clientDAO.persist(client);
        clientDAO.closeSession();
    }

    public Client findById(Integer id) {
        clientDAO.openSession();
        Client client = clientDAO.findById(id);
        clientDAO.closeSession();
        return client;
    }

    public void delete(Integer id) {
        clientDAO.openSession();
        Client client = clientDAO.findById(id);
        clientDAO.delete(client);
        clientDAO.closeSession();
    }

    public void update(Client client) {
        clientDAO.openSession();
        clientDAO.update(client);
        clientDAO.closeSession();
    }

    public List<Client> findAll() {
        clientDAO.openSession();
        List<Client> clients = clientDAO.findAll();
        clientDAO.closeSession();
        return clients;
    }

    public void deleteAll() {
        clientDAO.openSession();
        clientDAO.deleteAll();
        clientDAO.closeSession();
    }
}
