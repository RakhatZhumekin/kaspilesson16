package Project1.service;

import Project1.dao.ManufacturerDAO;
import Project1.dao.ManufacturerDAOImpl;
import Project1.entity.Manufacturer;

import java.util.List;

public class ManufacturerService {

    private static ManufacturerDAO<Manufacturer, Integer> manufacturerDAO;

    public ManufacturerService() {
        manufacturerDAO = new ManufacturerDAOImpl();
    }

    public void persist(Manufacturer manufacturer) {
        manufacturerDAO.openSession();
        manufacturerDAO.persist(manufacturer);
        manufacturerDAO.closeSession();
    }

    public Manufacturer findById(Integer id) {
        manufacturerDAO.openSession();
        Manufacturer manufacturer = manufacturerDAO.findById(id);
        manufacturerDAO.closeSession();
        return manufacturer;
    }

    public void delete(Integer id) {
        manufacturerDAO.openSession();
        Manufacturer manufacturer = manufacturerDAO.findById(id);
        manufacturerDAO.delete(manufacturer);
        manufacturerDAO.closeSession();
    }

    public void update(Manufacturer manufacturer) {
        manufacturerDAO.openSession();
        manufacturerDAO.update(manufacturer);
        manufacturerDAO.closeSession();
    }

    public List<Manufacturer> findAll() {
        manufacturerDAO.openSession();
        List<Manufacturer> manufacturers = manufacturerDAO.findAll();
        manufacturerDAO.closeSession();
        return manufacturers;
    }

    public void deleteAll() {
        manufacturerDAO.openSession();
        manufacturerDAO.deleteAll();
        manufacturerDAO.closeSession();
    }
}
