package Project1.service;

import Project1.dao.OrdersDAO;
import Project1.entity.Orders;
import Project1.dao.OrdersDAOImpl;

import java.util.List;

public class OrdersService {

    private static OrdersDAO<Orders, Integer> ordersDAO;

    public OrdersService() {
        ordersDAO = new OrdersDAOImpl();
    }

    public void persist(Orders order) {
        ordersDAO.openSession();
        ordersDAO.persist(order);
        ordersDAO.closeSession();
    }

    public Orders findById(Integer id) {
        ordersDAO.openSession();
        Orders order = ordersDAO.findById(id);
        ordersDAO.closeSession();
        return order;
    }

    public void delete(Integer id) {
        ordersDAO.openSession();
        Orders order = ordersDAO.findById(id);
        ordersDAO.delete(order);
        ordersDAO.closeSession();
    }

    public void update(Orders order) {
        ordersDAO.openSession();
        ordersDAO.update(order);
        ordersDAO.closeSession();
    }

    public List<Orders> findAll() {
        ordersDAO.openSession();
        List<Orders> orders = ordersDAO.findAll();
        ordersDAO.closeSession();
        return orders;
    }

    public void deleteAll() {
        ordersDAO.openSession();
        ordersDAO.deleteAll();
        ordersDAO.closeSession();
    }
}
