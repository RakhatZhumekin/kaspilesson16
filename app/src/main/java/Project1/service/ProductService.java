package Project1.service;

import Project1.dao.ProductDAO;
import Project1.dao.ProductDAOImpl;
import Project1.entity.Product;

import java.util.List;
import java.util.Map;

public class ProductService {

    private static ProductDAO<Product, Integer> productDAO;

    public ProductService() {
        productDAO = new ProductDAOImpl();
    }

    public void persist(Product product) {
        productDAO.openSession();
        productDAO.persist(product);
        productDAO.closeSession();
    }

    public Product findById(Integer id) {
        productDAO.openSession();
        Product product = productDAO.findById(id);
        productDAO.closeSession();
        return product;
    }

    public void delete(Integer id) {
        productDAO.openSession();
        Product product = productDAO.findById(id);
        productDAO.delete(product);
        productDAO.closeSession();
    }

    public void update(Product product) {
        productDAO.openSession();
        productDAO.update(product);
        productDAO.closeSession();
    }

    public List<Product> findAll() {
        productDAO.openSession();
        List<Product> products = productDAO.findAll();
        productDAO.closeSession();
        return products;
    }

    public void deleteAll() {
        productDAO.openSession();
        productDAO.deleteAll();
        productDAO.closeSession();
    }

//    public Map<Product, Integer> findMostPopularProducts(int limit) {
//        productDAO.openSession();
//        Map<Product, Integer> productsWithCounts = productDAO.findMostPopularProducts(limit);
//        productDAO.closeSession();
//        return productsWithCounts;
//    }
}
