create table client (
    id serial primary key,
    name varchar(100),
    address varchar(100)
);

create table orders (
    id serial primary key,
    client_id int,
    order_time date
);
alter table orders
add constraint fk_order_client
foreign key (client_id)
references client(id);

CREATE TABLE product (
	id serial primary key,
	name varchar(100),
	price int,
	manufacturer_id int
);

create table manufacturer (
    id serial primary key,
    name varchar(100),
    site_link varchar(100)
);
alter table product
add constraint fk_product_manufacturer
foreign key (manufacturer_id)
references manufacturer(id);

create table order_details (
    order_id int,
    product_id int
);
ALTER TABLE order_details ADD CONSTRAINT fk_order_product
    FOREIGN KEY (order_id)
    REFERENCES orders (id);
ALTER TABLE order_details ADD CONSTRAINT fk_product_order
    FOREIGN KEY (product_id)
    REFERENCES product (id);
